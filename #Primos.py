# Función para determinar si un número es primo o no.
#___autora___ = "Ingrid Guaraca"
#___email___ = "ingrid.guaraca@unl.edu.ec"

def num_primo (primo):
    if primo <= 1:
        return False
    elif primo == 2:
        return True
    else:
        for num in range(2, primo):
            if primo % num == 0:
                return False
        return True
try:
    if __name__ == "__main__":
        primo = int(input("Introduzca un número: "))
        número = num_primo(primo)
        if número is True:
            print("El número es primo")
        else:
            print("El número no es primo")
except:
    print ("Introduzca solo números enteros")


